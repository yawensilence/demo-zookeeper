package com.yawn.zk.config;

import com.yawn.zk.client.ZkClient;
import com.yawn.zk.props.ZkProps;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.RetryNTimes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ZkConfiguration {
    @Autowired
    ZkProps zkProps;

    @Bean(initMethod = "start")
    public CuratorFramework curatorFramework() {
        RetryNTimes retryNTimes =
                new RetryNTimes(zkProps.getRetryCount(), zkProps.getElapsedTimeMs());
        return CuratorFrameworkFactory.newClient(
                zkProps.getConnectString(),
                zkProps.getSessionTimeoutMs(),
                zkProps.getConnectionTimeoutMs(),
                retryNTimes
        );
    }

    @Bean
    public ZkClient zkClient() {
        return new ZkClient(zkProps.getConnectString());
    }
}
