package com.yawn.zk.props;

import java.util.Map;
import java.util.Properties;

public class SystemTest {

    public static void main(String[] args) {
        Properties properties = System.getProperties();
        properties.forEach((o, o2) -> System.out.println(o + ":\t" + o2));

        String osName = System.getProperty("os.name");
        String fileSeparator = System.getProperty("file.separator");
        String lineSeparator = System.getProperty("line.separator");
        String userHome = System.getProperty("user.home");
        String sunDesktop = System.getProperty("sun.desktop");

        System.out.println();
        Map<String, String> env = System.getenv(); // 系统环境变量
        env.forEach((k, v) -> System.out.println(k + ":\t" + v));
    }
}
