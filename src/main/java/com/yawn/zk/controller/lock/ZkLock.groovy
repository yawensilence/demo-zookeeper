package com.yawn.zk.controller.lock

import com.yawn.zk.client.ZkClient
import org.apache.zookeeper.CreateMode

class ZkLock {

    ZkClient zkClient
    String path

    ZkLock(ZkClient zkClient, String path) {
        this.zkClient = zkClient
        this.path = path
    }

    boolean acquire() {
        zkClient.createNode(path, CreateMode.EPHEMERAL, null)
    }

    boolean release() {
        if (zkClient.isExistNode(path)) {
            return zkClient.deleteNode(path)
        }
        true
    }

}
