package com.yawn.zk.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

@RestController
public class IpTestController {

    @GetMapping("ip")
    public Object ip(HttpServletRequest request) {
        StringBuffer url = request.getRequestURL();
        System.out.println(url);
        String remoteAddr = request.getRemoteAddr();
        String remoteHost = request.getRemoteHost();
        int remotePort = request.getRemotePort();
        System.out.println(remoteAddr + "\t" + remoteHost + "\t" + remotePort);
        String uri = request.getRequestURI();
        System.out.println(uri);
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String nextElement = headerNames.nextElement();
            System.out.println(nextElement + ":\t" + request.getHeader(nextElement));
        }
        return "dd";
    }
}
