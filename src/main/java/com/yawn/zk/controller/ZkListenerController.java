package com.yawn.zk.controller;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.api.CuratorWatcher;
import org.apache.curator.framework.api.GetDataBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("li")
public class ZkListenerController {

    @Autowired
    private CuratorFramework curator;
    private CuratorWatcher revocableWatcher = System.out::println;


    @GetMapping("tt")
    public String test() throws Exception {
        GetDataBuilder data = curator.getData();
        data.usingWatcher(revocableWatcher).forPath("/lock");
        return "aa";
    }
}
